#include <curses.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <ctime>

int x = 431424, y = 598356, r = 427089, px = 247872, py = 799248, pr,
    c = 348480, p = 615696, tick, board[20][10],
    block[7][4] = {
        {x, y, x, y},
        {r, p, r, p},
        {c, c, c, c},
        {599636, 431376, 598336, 432192},
        {411985, 610832, 415808, 595540},
        {px, py, px, py},
        {614928, 399424, 615744, 428369}},
    score = 0;

int NUM(int x, int y) {
    return 3 & block[p][x] >> y;
}

void new_piece() {
    y = py = 0;
    p = rand() % 7;
    r = pr = rand() % 4;
    x = px = rand() % (10 - NUM(r, 16));
}

void frame() {
    for (int i = 0; i < 20; i++) {
        move(1 + i, 1);
        for (int j = 0; j < 10; j++) {
            board[i][j] && attron(262176 | board[i][j] << 8);
            printw(" ");
            attroff(262176 | board[i][j] << 8);
        }
    }
    move(21, 1);
    printw("Score: %d", score);
    refresh();
}

void set_piece(int x, int y, int r, int v) {
    for (int i = 0; i < 8; i += 2) {
        board[NUM(r, i * 2) + y][NUM(r, (i * 2) + 2) + x] = v;
    }
}

int update_piece() {
    set_piece(px, py, pr, 0);
    set_piece(px = x, py = y, pr = r, p + 1);
}

void remove_line() {
    for (int row = y; row <= y + NUM(r, 18); row++) {
        c = 1;
        for (int i = 0; i < 10; i++) {
            c *= board[row][i];
        }
        if (!c) {
            continue;
        }
        for (int i = row - 1; i > 0; i--) {
            memcpy(&board[i + 1][0], &board[i][0], 40);
        }
        memset(&board[0][0], 0, 10);
        score++;
    }
}

int check_hit(int x, int y, int r) {
    if (y + NUM(r, 18) > 19) {
        return 1;
    }
    set_piece(px, py, pr, 0);
    c = 0;
    for (int i = 0; i < 8; i += 2) {
        board[y + NUM(r, i * 2)][x + NUM(r, (i * 2) + 2)] && c++;
    }
    set_piece(px, py, pr, p + 1);
    return c;
}

int do_tick() {
    if (++tick > 30) {
        tick = 0;
        if (check_hit(x, y + 1, r)) {
            if (!y) {
                return 0;
            }
            remove_line();
            new_piece();
        } else {
            y++;
            update_piece();
        }
    }
    return 1;
}

// main game loop with wasd input checking
void runloop() {
    while (do_tick()) {
        usleep(10000);
        if ((c = getch()) == 'a' && x > 0 && !check_hit(x - 1, y, r)) {
            x--;
        }
        if (c == 'd' && x + NUM(r, 16) < 9 && !check_hit(x + 1, y, r)) {
            x++;
        }
        if (c == 's') {
            while (!check_hit(x, y + 1, r)) {
                y++;
                update_piece();
            }
            remove_line();
            new_piece();
        }
        if (c == 'w') {
            ++r %= 4;
            while (x + NUM(r, 16) > 9) {
                x--;
            }
            if (check_hit(x, y, r)) {
                x = px;
                r = pr;
            }
        }
        if (c == 'q') {
            return;
        }
        update_piece();
        frame();
    }
}

// init curses and start runloop
int main() {
    srand(time(0));
    initscr();
    start_color();
    // colours indexed by their position in the block
    for (int i = 1; i < 8; i++) {
        init_pair(i, i, 0);
    }
    new_piece();
    resizeterm(22, 22);
    noecho();
    timeout(0);
    curs_set(0);
    box(stdscr, 0, 0);
    runloop();
    endwin();
}