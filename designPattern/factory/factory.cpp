#include <iostream>

using namespace std;

class Computer {
   public:
    virtual void price() = 0;
    virtual void description() = 0;
    virtual ~Computer() {}
};

class Laptop : public Computer {
   public:
    void price() {
        cout << "4000$" << endl;
    }
    void description() {
        cout << "Laptops are mobile, portable and multimedia" << endl;
    }
    virtual ~Laptop() {}
};

class Macbook : public Computer {
   public:
    void price() {
        cout << "6000$" << endl;
    }
    void description() {
        cout << "macbook is expensive, small and convient" << endl;
    }
    virtual ~Macbook() {}
};

class ComputerFactory {
   public:
    static Computer *NewComputer(const std::string &description) {
        if (description == "laptop") {
            return new Laptop;
        }
        if (description == "macbook") {
            return new Macbook;
        }
        return nullptr;
    }
};

int main(int argc, char const *argv[]) {
    Computer *com1 = ComputerFactory::NewComputer("laptop");
    com1->price();
    com1->description();

    Computer *com2 = ComputerFactory::NewComputer("macbook");
    com2->price();
    com2->description();

    return 0;
}
