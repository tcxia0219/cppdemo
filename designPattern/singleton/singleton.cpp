#include <iostream>

using namespace std;

class Singleton {
   public:
    static Singleton* getInstance() {
        if (_instance == 0) {
            _instance = new Singleton();
        }
        return _instance;
    }

   private:
    static Singleton* _instance;
    Singleton() {
        cout << "singleton..." << endl;
    }
};

Singleton* Singleton::_instance = 0;
int main(){
    Singleton* sgn1 = Singleton::getInstance();
    Singleton* sgn2 = Singleton::getInstance();
    delete sgn1;
    // delete sgn2; // 因为sgn1和sgn2共享同一指针，删除sgn2会报错
}