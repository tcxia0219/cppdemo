## 单例模式
让一个类只能实例化一个对象

## 案例
+ 将构造函数和指向Singleton实例的指针_instance设置为private
+ 当客户端调用getInstance时，判断_instance是否已经指向一个实例了