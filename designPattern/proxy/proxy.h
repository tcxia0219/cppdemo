#ifndef _proxy_
#define _proxy_

#include <iostream>
using namespace std;

class SchoolGirl {
   public:
    string getName();
    void setName(string name);

   private:
    string name;
};

class IGiveGift {
   public:
    IGiveGift();
    virtual ~IGiveGift();
    virtual void GiveDolls() = 0;
    virtual void GiveFlowers() = 0;
};

class Pursuit : public IGiveGift {
   public:
    Pursuit(SchoolGirl* mm);
    virtual ~Pursuit();
    virtual void GiveDolls();
    virtual void GiveFlowers();

   private:
    SchoolGirl* mm;
};

class Proxy : public IGiveGift {
   public:
    Proxy(SchoolGirl* mm);
    virtual ~Proxy();
    virtual void GiveDolls();
    virtual void GiveFlowers();

   private:
    Pursuit* gg;
};
#endif