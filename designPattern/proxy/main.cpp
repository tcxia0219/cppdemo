#include "proxy.h"

using namespace std;

int main() {
    SchoolGirl* girl = new SchoolGirl();
    girl->setName("meimei");
    Proxy* proxy = new Proxy(girl);
    proxy->GiveDolls();
    proxy->GiveFlowers();
}