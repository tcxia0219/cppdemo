## AbstractFactory
抽象工厂模式是为创建一组（有多类）**相关或依赖对象**提供创建接口，而Factory模式在相应的factory中分析的是一类对象提供创建接口。
AbstractFactory通常使用Factory模式实现，通俗来说，如果在Factory不仅仅买电脑，还买相机的话，而且相机操作和电脑操作有依赖关系，就可以考虑抽象工厂模式


## 案例解析
本案例主要通过后台切换数据库来展示工厂模式，因为在使用数据库时，会因为需求不同而切换不同的数据库
+ 抽象出所有操作的基础类IUser, IDepartment
+ 针对基础类定义接口
+ 基础类IUser的接口函数insert(), getUser()
+ 基础类IDepartment的接口函数insert(), getDepartment()
+ 数据库MysqlUser, SqlserverUser继承类IUser, 数据库MysqlDepartment, SqlserverDepartment继承基础类IDepartment来实现接口
+ 基础类抽象工厂类Factory来定义接口来实例化不同的数据库
+ SqlserverFactory和MysqlFactory继承Factory实现接口
+ 主函数通过声明一个指向基类Factory的指针来指向实际的子类SqlserverFactory和MysqlFactory实现，达到自由切换的目的