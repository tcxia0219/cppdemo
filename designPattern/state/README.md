## 状态模式
主要解决当控制一个对象状态转换的条件表达式比较复杂时的情况。把状态的判断逻辑转化到表示不同状态的一系列类当中，可以把复杂的判断逻辑简单化

## 案例
主要描述一个程序员一天不同的上班时间的不同状态，用状态模式表示
+ State基类定义接口，然后分别由AfternoonState, NoonState, ForenoonState继承这个基础类实现响应的接口
+ Work类设置时间函数setHour, 设置状态函数setState，获取时间getHour和写程序writeProgram，其中只有setHour开放给客户端使用