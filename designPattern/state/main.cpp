#include <iostream>

#include "state.h"
#include "work.h"

using namespace std;

int main() {
    Work* program = new Work();
    program->setHour(10);
    program->writeProgram();
    program->setHour(12);
    program->writeProgram();
    program->setHour(15);
    program->writeProgram();
    program->setHour(18);
    program->writeProgram();
    return 0;
}
