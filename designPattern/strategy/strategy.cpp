#include <iostream>
using namespace std;

class Strategy {
   public:
    Strategy() {}
    virtual ~Strategy() {}
    virtual void algorithmInterface() {}
};

class ConcreteStrategyA : public Strategy {
   public:
    ConcreteStrategyA() : Strategy() {}
    virtual ~ConcreteStrategyA() {}
    virtual void algorithmInterface() {
        cout << "algorithmInterfaceA" << endl;
    }
};

class ConcreteStrategyB : public Strategy {
   public:
    ConcreteStrategyB() : Strategy() {}
    virtual ~ConcreteStrategyB() {}
    virtual void algorithmInterface() {
        cout << "algorithmInterfaceB" << endl;
    }
};

class Context {
   public:
    Context(Strategy* strategy) {
        this->strategy = strategy;
    }
    void contextInterface() {
        this->strategy->algorithmInterface();
    }

   private:
    Strategy* strategy;
};

int main() {
    Context* context;
    context = new Context(new ConcreteStrategyA());
    context->contextInterface();
    
    context = new Context(new ConcreteStrategyB());
    context->contextInterface();
}