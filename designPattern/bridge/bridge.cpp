#include <iostream>

using namespace std;

class Handsetsoft {
   public:
    Handsetsoft() {}
    virtual ~Handsetsoft() {}
    virtual void run() = 0;
};

class HandsetGame : public Handsetsoft {
   public:
    HandsetGame() : Handsetsoft() {}
    virtual ~HandsetGame() {}
    virtual void run() {
        cout << "run game" << endl;
    }
};

class HandsetAddress : public Handsetsoft {
   public:
    HandsetAddress() : Handsetsoft() {}
    virtual ~HandsetAddress() {}
    virtual void run() {
        cout << "run address" << endl;
    }
};

class HandsetBrand {
   public:
    HandsetBrand() {}
    virtual ~HandsetBrand() {
        delete soft;
    }
    virtual void setHandsetsoft(Handsetsoft* soft) {
        this->soft = soft;
    }
    virtual void run() {}

   protected:
    Handsetsoft* soft;
};

class Iphone : public HandsetBrand {
   public:
    Iphone() : HandsetBrand() {}
    virtual ~Iphone() {}
    virtual void run() {
        soft->run();
    }
};

class Android : public HandsetBrand {
   public:
    Android() : HandsetBrand() {}
    virtual ~Android() {}
    virtual void run() {
        soft->run();
    }
};

int main(int argc, char const* argv[]) {
    HandsetBrand* iphone = new Iphone();
    iphone->setHandsetsoft(new HandsetGame());
    iphone->run();

    HandsetBrand* android = new Android();
    android->setHandsetsoft(new HandsetAddress());
    // android->setHandsetsoft(new HandsetGame());
    android->run();
    return 0;
}
