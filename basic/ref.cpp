#include <iostream>
using namespace std;

int sum1(int &v1, int &v2);
int sum2(const int &v1, const int &v2);


// const和非const构成重载
int sum(int &v1, int &v2){
	cout << "sum(int &v1, int &v2)" << endl;
	return v1 + v2;
}// 1

int sum(const int &v1, const int &v2){
	cout << "sum(const int &v1, const int &v2)" << endl;
	return v1 + v2 + v1;
} // 2

 // int sum(int v1, int v2){
 // 	cout << "sum(int v1, int v2)" << endl;
 // 	return v1 + v2 + v2;
 // } // 3



int main(int argc, char const *argv[])
{
	int age = 10;
	int &ref = age;
	ref += 10; // 相当于直接对age做加法
	cout << "age: " << age << endl;
	cout << "ref: "<< ref << endl;

	int &refN = ref; // 使用引用初始化另一个引用
	cout << "refn: " << refN << endl;

	int height = 200;
	ref = height;
	cout << ref << endl;

	// &ref = height; // error

	int *p = &age; // 定义指针， *p就是age的别名
	*p = 50; // 直接修改了age的值
	cout << "age: " << age << endl;
	cout << "*p: " << *p << endl;
	cout << "p: " << p << endl;

	p = &height; // 可修改 对比19行
	cout << "age: " << age << endl;
	cout << "*p: " << *p << endl;
	cout << "p: " << p << endl; 


	int age2 = 20;
	const int &ref2 = age2; // const必须写在&左边
	// ref2 = height; // error 常引用无法更改值
	cout << "ref2: " << ref2 << endl;
	cout << "age2: " << age2 << endl;


	int ageD = 10;
	// double &refD = ageD; // error 
	const double &refcD = ageD; // 会产生临时变量
	ageD = 30;
	cout << "ageD: " << ageD << endl; // 30 
	cout << "refcD: " << refcD << endl; // 10



 
 	int a = 10;
 	int b = 20;
 	int ret1 = sum1(a, b); // 不可以直接传值
 	cout << "ret1: " <<ret1 << endl;

 	int ret2 = sum2(30, 20); // 可以传值
 	int ret3 = sum1(a, b);
 	cout << "ret2: " << ret2 << endl;
 	cout << "ret3: " << ret3 << endl;


 	cout << sum(10, 20) << endl; // error, 调用2和3不确定
 	cout << sum(a, b) << endl;

 	// 数组名arr其实就是数组的地址，也是数组首元素的地址
 	// 数组名arr可以看做是指向数组首元素的指针(int *)
 	int arr[] = {1, 10, 3};
 	int * const &refa = arr;
 	cout << "arr: " << arr << endl;
 	cout << "arr[1]: " << arr[1] << endl;
 	cout << "refa: " << refa << endl;
 	cout << "refa[1]: " << refa[1] << endl;


	return 0;
}

int sum1(int &v1, int &v2){
	return v1 + v2;
}

int sum2(const int &v1, const int &v2){
	return v1 + v2;
}


