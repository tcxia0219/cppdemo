#include <iostream>
    
using namespace std;

/**
 * 内联函数，本质上就是将函数体直接调用
 *  1. 函数代码体积不大
 *  2. 频繁调用的函数
 *  3. 不适合递归
 */ 

// 适合
inline void func(){
    cout << "func()" << endl;
}

// 适合
inline int sum(int v1, int v2){
    return v1 + v2;
}

// 不适合
inline void run(){
    run();
}


int main(int argc, char const *argv[])
{
    int a = 1;
    int b = 2;
    (a > b ? a : b) = 4;
    cout << a << endl; // 1
    cout << b << endl; // 4
    
    return 0;
}
