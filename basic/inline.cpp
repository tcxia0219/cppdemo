#include <iostream>
using namespace std;

// 编译时候将函数直接展开为函数体代码
inline int sum(int x){
	return x + x;
}

int main(int argc, char const *argv[])
{
	cout << sum(10) << endl;
	return 0;
}