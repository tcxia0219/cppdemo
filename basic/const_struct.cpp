#include <iostream>
using namespace std;


struct Student{
	int age;
};

int main(int argc, char const *argv[])
{
	Student stu1 = {10};
	Student stu2 = {20};

	const Student *pStu1 = &stu1; // pStu1可修改, *pStu1不可修改
	cout << "pStu1: " << pStu1 << endl;
	// cout << "pStu1->age: " << &(pStu1->age) << endl;
	// *pStu1 = stu2; // error
	pStu1 = &stu2; // 
	// (*pStu1).age = 30; // error
	// pStu1->age = 40; // error

	cout << "pStu1" << pStu1 << endl;


	Student *const pStu2 = &stu1;
	*pStu2 = stu2; 
	// pStu2 = &stu2; // error
	(*pStu2).age = 40;
	pStu2->age = 50;
	
	return 0;
}






