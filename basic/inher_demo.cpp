#include <iostream>
    
using namespace std;

/**
 * 成员访问权限 
 *  1. public(都可以访问), protected(当前类以及子类), private(当前类)
 *  2. 
 */

struct Person{
private:
    int m_age;

protected:
    int m_num;

public:
    int m_blood;
};

struct Student : public Person{
    int m_id;
    void run(){
        // cout << m_age << endl; // 不能访问
        cout << m_num << endl; // 可以访问
        cout << m_blood << endl; // 可以访问
    }
};

struct GoodStudent : public Student{
    int m_money;
};

void test(){
    GoodStudent gs;
    cout << sizeof(gs) << endl; // 12个字节
}

struct Animal{
    // 虚函数 ------> 虚表
    virtual void speak(){
        cout << "Animal::speak()" << endl;
    }
    virtual void run(){
        cout << "Animal::run()" << endl;
    }
};

struct Dog : public Animal{
    void speak(){
        cout << "Dog::speak()" << endl;
    }

    void run(){
        cout << "Dog::run()" << endl;
    }
};

struct Cat : public Animal{
    void speak(){
        cout << "Cat::speak()" << endl;
    }

    void run(){
        cout << "Cat::run()" << endl;
    }
};

void demo(Animal *p){
    p->run();
    p->speak();
}

void test1(){
    // 多态
    // 指向Cat对象
    Animal *p = new Cat(); // 父类指针指向子类对象
    p->run();
    p->speak();

    // 指向Dog对象
    Animal *p1 = new Dog();
    p1->run();
    p1->speak();

    demo(p);
    demo(p1);
}

// 抽象类
struct Animal_{
    // 纯虚函数
    virtual void speak() = 0;
    virtual void run() = 0;
};

struct Dog_ : Animal_{
    void speak(){
        cout << "Dog_::speak()" << endl;
    }

    void run(){
        cout << "Dog_::run()" << endl;
    }
};

void test2(){
    Animal_ *p =  new Dog_();
    p->run();
    p->speak();
}

struct Person1{
    int m_age = 1;
};
// 虚继承
struct Student1 : virtual Person1{
    int m_id = 20;
};

struct Worker1 : virtual Person1{
    int m_salary = 30;
};

struct Graduate1 : Student1, Worker1{
    int m_grade = 4;
};


int main(int argc, char const *argv[])
{
    Graduate1 gd;
    cout << sizeof(gd) << endl; // 40
    return 0;
}
