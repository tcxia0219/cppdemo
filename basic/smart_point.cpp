#include <iostream>
#include <memory>
    
using namespace std;

/**
 *  普通指针创建，一般来说需要手动销毁，智能指针会自动销毁
 * 
 *  auto_ptr: 不推荐使用，不能用于数组, 避免潜在的内存崩溃问题, double free问题
 *  share_ptr:
 *  1. 多个shared_ptr可以指向同一个对象，当最后一个shared_ptr在作用域范围内结束时，对象才会被自动释放
 *  2. 可以通过已经存在的智能指针初始化一个新的智能指针
 *  3. 一个share_ptr会对一个对象产生强引用， 每个对象都有个与之对应的强引用计数，记录着当前对象被多少个share_ptr强引用着
 *      可以通过use_count()获得强引用计数
 *  4. 当有一个新的share_ptr指向对象时，对象的强引用计数就会+1
 *  5. 当有一个share_ptr被销毁时，对象的强引用计数就会-1
 *  6. 当对象的强引用计数变成0时，对象就会自动销毁
 * 
 *  weaked_ptr: 会对一个对象产生弱引用, 可以指向对象解决shared_ptr的循环引用问题
 *  unique_ptr: 也会对对象产生一个强引用, 它可以确保同一时间只有一个1个指针指向对象
 *  1. 当unique_ptr销毁时，其指向的对象也自动销毁了
 *  2. 可以使用std::move 转移unique_ptr的所有权
 */

class Person{
private:
    int m_age;
public:
    Person(){
        cout << "Person()" << endl;
    }

    Person(int age) : m_age(age){
        cout << "Person(int)" << endl;
    }

    ~Person(){
        cout << "~Person()" << endl;
    }

    void run(){
        cout << "run() - " << m_age << endl;
    }
};


// 自定义智能指针
template <typename T>
class SmartPoint{
private:
    T* m_obj;

public:
    SmartPoint(T *obj) : m_obj(obj){}
    ~SmartPoint(){
        if (m_obj == nullptr) return;
        delete m_obj;
    }

    // 重载"->", 才可以访问成员函数
    T *operator->(){
        return m_obj;
    }
};

// 循环引用
class Teacher;

class Car{
public:
    // shared_ptr<Teacher> m_teacher = nullptr;
    weak_ptr<Teacher> m_teacher;
};

class Teacher{
public:
    shared_ptr<Car> m_car = nullptr;
};


unique_ptr<Person> test(const Person *person){
    unique_ptr<Person> temp(new Person());
    return temp;
}

void test(){
    // Person *p = new Person();
    // p->run();
    // throw ""; 抛出, 即delete将不会被执行，造成内存泄露
    // delete p;

    cout << "=" << endl;
    {
        // auto_ptr<Person> p1 = new Person(); // 报错
        auto_ptr<Person> p1(new Person()); // 智能指针p指向了堆空间的Person对象
        // auto_ptr<Person> p1(new Person[]); // 报错，不能指向指针数组
        p1->run();
    }

    cout << "==" << endl;
    {
        SmartPoint<Person> p2(new Person());
        p2->run();
    }

    cout << "===" << endl;
    {
        shared_ptr<Person> p3(new Person());
        cout << p3.use_count() << endl; // 1
        shared_ptr<Person> p4 = p3;
        cout << p3.use_count() << endl; // 2
        shared_ptr<Person> p5(p3);
        cout << p3.use_count() << endl; // 3

        {
            shared_ptr<Person> p6 = p3;
            cout << p3.use_count() << endl; // 4
        }
        cout << p3.use_count() << endl; // 3
    }
    // double free 出现问题
    // {
    //     Person *p = new Person();
    //     {
    //         shared_ptr<Person> p1(p);
    //     } // ~Person()
    //     {
    //         shared_ptr<Person> p2(p);
    //     } // ~Person()
    // }

    cout << "====" << endl;
    // 循环引用
    // 两个对象相互引用，永远也不会销毁，使用策略weak_ptr
    shared_ptr<Teacher> teach(new Teacher());
    shared_ptr<Car> car(new Car());
    teach->m_car = car;
    car->m_teacher = teach;
    cout << teach.use_count() << endl; // 2
    cout << car.use_count() << endl; // 2


    unique_ptr<Person> pt1(new Person());
    // unique_ptr<Person> pt2 = pt1; // 报错
    // unique_ptr<Person> pt3(pt1); // 报错
    unique_ptr<Person> pt2 = std::move(pt1); // 转移 pt1--->pt2

    unique_ptr<Person> pt3;
    pt3 = test(new Person()); // 编译器允许，右值是临时的

    // delete p;
}


template <typename T>
class Auto_ptr1{
public:
    Auto_ptr1(T* ptr = nullptr): m_ptr{ptr}{}
    
    // 虚析函数
    virtual ~Auto_ptr1(){
        delete m_ptr;
    }

    T& operator*(){
        return *m_ptr;
    }

    T* operator->(){
        return m_ptr;
    }

private:
    T* m_ptr;
};

class Resource{
public:
    Resource(){
        cout << "Resource acquired!" << endl;
    }
    virtual ~Resource(){
        cout << "Resource destoryed!" << endl;
    }

};

template <typename T>
class Auto_ptr2{
public:
    Auto_ptr2(T* ptr = nullptr) : m_ptr{ptr} {}

    virtual ~Auto_ptr2(){
        delete m_ptr;
    }

    // 对象所有权转移
    Auto_ptr2(Auto_ptr2& rhs){
        m_ptr = rhs.m_ptr;
        rhs.m_ptr = nullptr;
    }

    Auto_ptr2& operator=(Auto_ptr2& rhs){
        if(&rhs == this) return *this;

        delete m_ptr;
        m_ptr = rhs.m_ptr;
        rhs.m_ptr = nullptr;
        return *this;
    }

    T& operator*() { return *m_ptr; }
    T* operator->(){ return m_ptr; }
    bool isNull() const{
        return m_ptr == nullptr;
    }

private:
    T* m_ptr;
};


// 仅可以传递右值，但是不能传递左值，可以将右值传递给函数的const左值引用参数
template <typename T>
class Auto_ptr3{
public:
    Auto_ptr3(T* ptr = nullptr): m_ptr{ptr}{}
    
    Auto_ptr3(const Auto_ptr3& rhs) = delete;

    Auto_ptr3(Auto_ptr3&& rhs) : m_ptr{rhs.m_ptr}{
        rhs.m_ptr = nullptr;
    }

    // 禁用了复制构造函数和复制赋值运算符
    Auto_ptr3& operator=(const Auto_ptr3& rhs) = delete;
    Auto_ptr3& operator=(Auto_ptr3&& rhs){
        if(this == &rhs){
            return *this;
        }
        swap(m_ptr, rhs.m_ptr);
        return *this;
    }

    virtual ~Auto_ptr3(){
        delete m_ptr;
    }

    T& operator*(){ return *m_ptr; }
    T* operator->() { return m_ptr; }

    bool isNull() const { return m_ptr == nullptr; }

private:
    T* m_ptr;
};

int main(int argc, char const *argv[])
{   
    // 1
    {
        Auto_ptr1<Resource> res(new Resource);
        // Resource acquired!
        // Resource destoryed!
    }

    // 2 segmentation fault
    // {
    //     Auto_ptr1<Resource> res1(new Resource);
    //     // 调用了默认构造函数，并且浅拷贝，res2和res1内部保存的是同一块内存，当销毁变量时，同一块内存将会被多次释放
    //     // double free
    //     Auto_ptr1<Resource> res2(res1); 
    // } 
    
    // 3 对象指针转移, 移动语义
    {
        Auto_ptr2<Resource> res1(new Resource());
        Auto_ptr2<Resource> res2;
        cout << "res1 is " << (res1.isNull() ? "null \n" : "not null \n");
        cout << "res2 is " << (res2.isNull() ? "null \n" : "not null \n");

        res2 = res1;
        cout << "res1 is " << (res1.isNull() ? "null \n" : "not null \n");
        cout << "res2 is " << (res2.isNull() ? "null \n" : "not null \n");
    }
    return 0;
}
