#include<iostream>
#include<cstring>
    
using namespace std;

/*
    程序的错误一般分为3种
    1. 语法错误
    2. 逻辑错误
    3. 运行时错误（异常）

    throw异常后, 会在当前函数中查找匹配的catch, 找不到就终止当前函数代码，去上一层函数中查找。
    如果最终找不到，整个程序就会终止

*/




// 自定义异常1
class OutOfRange{
private:
    int m_flag;
    int m_len;
    int m_index;

public:
    OutOfRange() : m_flag(1){}
    OutOfRange(int len, int index) : m_len(len), m_index(index), m_flag(2){}

    void what() const{
        if (m_flag == 1){
            cout << "Error: empty array, no element to pop." << endl;
        }else if (m_flag == 2){
            cout << "Error: out of range( array length " << m_len << ", access index" << m_index << " )" << endl;
        }else{
            cout << "Unknown exception." << endl;
        }
    }
};


// 变长数组类
class Array{
private:
    int m_size;
    int m_capacity;
    int *m_data;
public:
    Array(){}
    ~Array(){
        if(m_data)
            delete[] m_data;
    }

    int operator[](int index){
        if (index < 0 || index > m_size)
            throw OutOfRange(m_size, index);
        return m_data[index];
    }

    int push(int value){
        if (m_size == m_capacity){
            int *temp = new int[m_capacity + 50];
            memcpy(temp, m_data, sizeof(int) * m_size);
            delete[] m_data;
            m_data = temp;
        }
        m_data[m_size++] = value;
    }

    int pop(){
        if (m_size == 0)
            throw OutOfRange();
        return m_data[m_size--];
    }

};

// throw写在函数头和函数体之间，指明当前函数需要抛出的异常类型，称为异常规范
int devide1(int a, int b) throw (int){
    if (b == 0) throw 404;
    return a / b;
}

// 异常规范在函数声明和和函数定义中必须同时指出，并且保持严格一致
class Base{
public:
    virtual void func1() throw();
    virtual void func2() throw(int);
    virtual string func3() throw(int, string);
};

class SubBase : public Base{
public:
    // void func1() throw(int){} // 错误， 异常规范不严格
    void func2() throw(int){} // 正确， 有相同的异常规范
    string func3() throw (string){} // 正确，异常规范比throw(int, string)更严格
};

/*
    exception: 标准异常类
    try{
        // 可能抛出异常的语句
    }catch(exception &e){ // 之所以用引用，就是为了提高效率，不使用引用，经历一次对象拷贝（拷贝构造函数）
        // 处理异常的语句
    }

*/

// 定义异常类型
void test1(){
    double a = 10;
    double b = 0;
    try{
        cout << devide1(a, b) << endl;
    }catch(int exception){
        cout << "异常发生: " << exception << endl;
    }
}

// 自定义异常类型
class Exception{
public:
    virtual string what() const = 0;  
};

class DevideException : public Exception{
public:
    string what() const {
        return "除数不能为0";
    }
};

int devide2(int a, int b) throw (Exception){
    if (b == 0) throw DevideException();
    return a / b;
}

int main(int argc, char const *argv[])
{
    try{
        int a = 10;
        int b = 0;
        int c = devide2(a, b);
        cout << 1 << endl; // 不会被执行
    }catch(const DevideException &e){ // catch(...)表示拦截所有类型的异常
        cout << "发生异常: " << e.what() << endl; 
    }
    
    return 0;
}
