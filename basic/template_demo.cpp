#include<iostream>
#include<cstring>

using namespace std;

/*
    函数模板
*/
template <typename T>
T add(T a, T b){
    return a + b;
}

class Point{
private:
    friend ostream &operator<<(ostream &, const Point &);
    int m_x;
    int m_y;
public:
    Point(int x = 0, int y = 0) : m_x(x), m_y(y){}

    // 运算符重载
    Point operator+(const Point &point){
        return Point(point.m_x + this->m_x, point.m_y + this->m_y);
    }
};

// 操作符重载
ostream &operator<<(ostream &cout, const Point &point){
    cout << "(" << point.m_x << "," << point.m_y << ")";
    return cout;
}

// 动态数组
/*
    动态数组改成 ---> 模板类
*/

template <typename Item>
class Array;

template <typename Item>
ostream &operator<<(ostream &cout, Array<Item> &array);

template <typename Item>
class Array{
private:
    friend ostream &operator<<(ostream &, const Array<Item> &);

    Item *m_data;  // 动态申请空间指针，用于指向首元素
    int m_size; // 元素个数
    int m_capacity; // 容量

public:
    Array(int capacity = 0){
        m_capacity = (capacity > 0) ? capacity : 10;
        m_data = new Item[m_capacity]; // 申请指定容量的capacity
    }

    ~Array(){
        if (m_data == nullptr) return;
        delete[] m_data; // 释放堆空间
    }

    void checkIndex(int index){
        // cout << m_size << endl;
        if (index < 0 || index >= m_size)
        {
            throw "数组下标越界";
        }
    }

    void add(Item value){
        // cout << m_capacity << endl;
        if (m_size == m_capacity)
        {
            cout << "空间不够, 需要扩容" << endl;
            /*
                扩容
                1. 申请新的空间
                2. 拷贝旧数据到新的栈空间
                3. 删除旧栈空间
                4. 赋值新的栈空间
            */

            Item *temp = new Item[m_capacity + 2];
            // for (int i = 0; i < m_size; i++){
            //     temp[i] = m_data[i];
            // }
            memcpy(temp, m_data, sizeof(Item) * m_size); // 拷贝原数据到新的栈空间
            delete[] m_data;
            m_data = temp;
            // m_capacity = m_capacity * 2;
            // // cout << sizeof(*m_data) << endl;
            // m_data[m_size++] = value;
            // return;
        }

        // cout << "插入元素" << endl;
        m_data[m_size++] = value;
    }

    Item getValue(int index){
        checkIndex(index);
        return m_data[index];
    }

    Item operator[](int index){
        // return getValue(index);
        checkIndex(index);
        return m_data[index];
    }
};

template <typename Item>
ostream &operator<<(ostream &cout, const Array<Item> &array){
    cout << "[";
    for (int i = 0; i < array.m_size; i++){
        if (i != 0)
            cout << ", ";
        cout << array.m_data[i];
    }

    return cout << "]";
}

// 动态数组 
class CArray{
private:
    int m_size;
    int *m_data;
public:
    CArray(int capacity) : m_size(capacity){
        if (capacity == 0)
            m_data = nullptr;
        else
            m_data = new int[capacity]; // 申请堆空间
    }
    
    CArray(const CArray &carry){
        if (!carry.m_data){
            m_data = nullptr;
            m_size = 0;
            return;
        }
        m_data = new int[carry.m_size];
        memcpy(m_data, carry.m_data, sizeof(int) * carry.m_size);
        m_size = carry.m_size;
    }

    ~CArray(){
        if(m_data)
            delete[] m_data;
    }

    void push_back(int value){
        if (m_data){
            int *temp = new int[m_size + 1];
            memcpy(temp, m_data, sizeof(int) * m_size);
            delete[] m_data;
            m_data = temp;
        }
        else{
            m_data = new int[1];
        }
        m_data[m_size++] = value;
    }

    int &operator[](int index){
        return m_data[index];
    }
};

int main(int argc, char const *argv[])
{
    // cout << add(10, 100) << endl;
    // cout << add(10.1, 100.5) << endl;
    // cout << add(Point(12, 14), Point(10, 20)) << endl;

    // Array arr;
    // arr.add(10);
    // arr.add(12);
    // cout << arr[1] << endl;

    // Array<Point> arr(15);
    // arr.add(Point(1, 2));
    // arr.add(Point(3, 4));
    // cout << arr << endl;

    int arr2[]{1, 2, 3, 4};
    for( auto i : arr2){
        cout << i << endl;
    }

    return 0;
}

