#include <iostream>
    
using namespace std;

class Person{
public:
    int m_id;
    int m_age;
    int m_height;

public:
    // this指针存储着函数调用者的地址
    // this指向函数调用者
    void display(){
        cout << "m_id: " << this->m_id << endl;
        cout << "m_age: " << this->m_age << endl;
        cout << "m_height: " << this->m_height << endl;
    }
};


int main(int argc, char const *argv[])
{
    // person对象在内存的栈空间, 并且未初始化
    Person person;
    person.m_id = 10;
    person.m_age = 20;
    person.m_height = 30;
    // 将person对象的地址传递给display函数的this
    // person.display(); // 10 20 30

    // Person *p = &person; // 指针指向对象
    // p->m_id = 40;
    // p->m_age = 30;
    // p->m_height = 50;
    // p->display(); // 40 20 50

    Person *p1 = (Person *) &person.m_age;
    p1->m_id = 40;
    p1->m_age = 50;
    // 将指针p1里面存储的地址传递给display函数的this
    // 即将person.m_age传递给display的this
    p1->display(); // 40 50 0
    
    return 0;
}
