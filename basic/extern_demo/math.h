#ifndef __MATH_H // 多次调用，只初始化一次
#define __MATH_H

#ifdef __cplusplus // 在C语言调用时不生效，在C++调用时生效
extern "C" {
#endif

int sum(int v1, int v2);
int delta(int v1, int v2);
int divide(int v1, int v2);

#ifdef __cplusplus
}
#endif


#endif