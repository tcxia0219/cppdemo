#include <iostream>
using namespace std;


// extern "C" {
// 	#include "sum.h"
// }

/*
编译过程:
 g++ -c extern.cpp
 gcc -c math.c
 g++ -o extern extern.o math.o
*/

#include "math.h"

int main()
{
	cout << sum(10, 20) << endl;
	cout << delta(30, 20) << endl;
	cout << divide(20, 20) << endl;
	return 0;
}