#include <iostream>

using namespace std;

// 分开写
extern "C" void func(){
	cout << "func() - 123" << endl;
}

extern "C" void func(int v){
	cout << "func(int v) - 123" << endl;
}

// 合并写
extern "C" {
	void func(){
		cout << "func() - 1234" << endl;
	}

	void func(int v){
		cout << "func(int v) - 1234" << endl;
	}
}



int main(int argc, char const *argv[])
{
	func();
	return 0;
}