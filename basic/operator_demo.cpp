#include <iostream>
#include <iomanip>

using namespace std;

// size_t 定义为 typedef unsigned int size_t 无符号整型

/*
    重载new/new[]
        1. 返回值都是 void * 类型
        2. 第一个参数类型固定为 size_t, new表示要分配空间的大小， new[]表示所需要分配的所有空间的总和 
    
    重载delete/delete[]
        1. 返回值都是 void 类型
        2. 必须有一个void类型的指针作为参数，该指针指向需要释放的内存空间
*/

// 点类
class Point
{
private:
    friend Point add(const Point &, const Point &); // 友元函数
    friend Point operator+(const Point &, const Point &);
    friend Point operator-(const Point &, const Point &);
    friend ostream &operator<<(ostream &, const Point &);

    int m_x;
    int m_y;

public:
    Point(int x, int y) : m_x(x), m_y(y) {}

    Point(const Point &point)
    {
        this->m_x = point.m_x;
        this->m_y = point.m_y;
    }

    void display()
    {
        cout << "(" << m_x << "," << m_y << ")" << endl;
    }

    // 前置++   ++i
    Point &operator++()
    {
        ++m_x;
        ++m_y;
        return *this;
    }

    // 后置++   i++
    const Point operator++(int)
    {
        Point old = *this;
        m_x++;
        m_y++;
        return old;
    }

    const Point operator-() const
    {
        return Point(-m_x, -m_y);
    }

    bool operator!=(const Point &point) const
    {
        return (m_x != point.m_x) || (m_y != point.m_y);
    }

    bool operator==(const Point &point) const
    {
        return (m_x == point.m_x) && (m_y == point.m_y);
    }

    Point &operator+=(const Point &point)
    {
        m_x += point.m_x;
        m_y += point.m_y;
        return *this;
    }
    Point operator+(const Point &point)
    {
        return Point(point.m_x + m_x, point.m_y + m_y);
    }

    Point operator-(const Point &point)
    {
        return Point(m_x - point.m_x, m_y - point.m_y);
    }
};

Point add(const Point &p1, const Point &p2)
{
    return Point(p1.m_x + p2.m_x, p1.m_y + p2.m_y);
}

Point operator+(const Point &p1, const Point &p2)
{
    return Point(p1.m_x + p2.m_x, p1.m_y + p2.m_y);
}

Point operator-(const Point &p1, const Point &p2)
{
    return Point(p1.m_x - p2.m_x, p1.m_y - p2.m_y);
}

ostream &operator<<(ostream &cout, const Point &point)
{
    cout << point.m_x << " " << point.m_y;
    return cout;
}

// 表类
class stopwatch
{
private:
    int m_min; // 分钟
    int m_sec; // 秒钟
public:
    void setzero()
    {
        m_min = 0;
        m_sec = 0;
    }
    stopwatch() : m_min(0), m_sec(0) {}
    stopwatch run();           // 运行，秒针前进一秒的动作
    stopwatch operator++();    // ++i, 前置
    stopwatch operator++(int); // i++, 后置
    friend ostream &operator<<(ostream &, const stopwatch &);
};

stopwatch stopwatch::run()
{
    ++m_sec;
    if (m_sec == 60)
    {
        m_min++;
        m_sec = 0;
    }
    return *this;
}

stopwatch stopwatch::operator++()
{
    return run();
}

// 后置自增，返回值是对象本身，之后再次使用该对象时，对象自增了，所以在函数体中先将对象保存，然后自增，再将先前保存的对象返回
stopwatch stopwatch::operator++(int)
{
    stopwatch s = *this;
    run();
    return s;
}

ostream &operator<<(ostream &cout, const stopwatch &s)
{
    cout << setfill('0') << setw(2) << s.m_min << ":" << setw(2) << s.m_sec;
    return cout;
}

// 复数类
class Complex
{
private:
    double m_real;
    double m_imag;

    friend ostream &operator<<(ostream &, const Complex &);
    friend istream &operator>>(istream &, Complex &); // 参数可更改，不能用const

public:
    Complex(double r = 0.0, double m = 0.0) : m_real(r), m_imag(m) {}

    Complex operator+(const Complex &complex)
    {
        return Complex(this->m_real + complex.m_real, this->m_imag + complex.m_imag);
    }

    bool operator==(const Complex &complex)
    {
        return (this->m_real == complex.m_real) && (this->m_imag == complex.m_imag);
    }

    Complex operator+=(const Complex &complex)
    {
        m_real += complex.m_real;
        m_imag += complex.m_imag;
        return *this;
    }

    // 重载() , 相当于强制类型转换
    operator double()
    {
        return m_real;
    }

    double getReal() const
    {
        return m_real;
    }

    double getImag() const
    {
        return m_imag;
    }
};

ostream &operator<<(ostream &cout, const Complex &complex)
{
    cout << complex.m_real << " " << complex.m_imag;
    return cout;
}

istream &operator>>(istream &cin, Complex &complex)
{
    cin >> complex.m_real >> complex.m_imag;
    return cin;
}

// 数组类
class Array
{
private:
    int m_size;
    int *m_data;
    int m_capacity;

public:
    Array(int capacity = 0)
    {
        m_capacity = (capacity > 0) ? capacity : 5;
        m_data = new int[m_capacity];
    }
    ~Array()
    {
        if (m_data)
            delete[] m_data;
    }

    /*
        这两种方式最好都提供
        第一种方式 不仅可以访问元素，还可以修改元素
        第二种方式 只能访问元素而不可以修改元素
    */
    int &operator[](int index)
    {
        return m_data[index];
    }

    const int &operator[](int index) const
    {
        return m_data[index];
    }
};

// 仿函数
class Sum
{
public:
    int m_age;

    int operator()(int a, int b)
    {
        return a + b;
    }

    void func(){
        m_age = 10;
    }
};

int main(int argc, const char **argv)
{

    Sum sum;
    cout << sum(10, 20) << endl; // sum.operator()(10, 20)


    // Point p1(10, 20);
    // Point p2(30, 40);
    // Point p3(40, 100);
    // // Point p3 = add(p1, p2);
    // // Point p4 = p1 + p2 + p3;
    // Point p4 = p3 - p1;
    // // p4.display();

    // cout << p1 << endl;

    // // cout << ++p1 << endl; // 11 21
    // cout << p1++ << endl; // 11 21

    // stopwatch s1, s2;
    // s1 = s2++;
    // cout << "s1:" << s1 << endl;
    // cout << "s2:" << s2 << endl;
    // s1.setzero();
    // s2.setzero();

    // s1 = ++s2;
    // cout << "s1:" << s1 << endl;
    // cout << "s2:" << s2 << endl;

    // Complex c1(2.5, 5.4);
    // Complex c2(2.3, 1.4);
    // cout << double(c1) << endl;

    // // Complex c3;
    // // cin >> c3;
    // // cout << c3 << endl;

    // cout << c1 + c2 << endl;
    // cout << (c1 == c2) << endl;
    // c2 += c1;
    // cout << c1 << endl;
    // cout << c2 << endl;

    return 0;
}