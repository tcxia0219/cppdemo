#include <iostream>
using namespace std;


/*
 * C语言不支持重载
 * C++支持重载
 */

int sum(int v1, int v2){
    return v1 + v2;
}

// 参数数量
int sum(int v1, int v2, int v3){
    return v1 + v2 + v3;
}


void func(int v1, double v2){
    cout << "func(int v1, double v2)" << endl;
}

// 参数类型
void func(double v1, int v2){
    cout << "func(double v1, int v2)" << endl;
}


// display_v
void display(){
    cout << "display()" << endl;
}

// display_i
void display(int a){
    cout << "display(int a)" << endl;
}

// display_l
void display(long a){
    cout << "display(long a)" << endl;
}

// display_d
void display(double a){
    cout << "display(double a)" << endl;
}

// 歧义，二义性
// int func(int a){
//     return 0;
// }

// double func(){
//     return 0;
// }


int main(){
    sum(10, 11);
    sum(10, 11, 12);


    display();
    display(10);
    display(10L);
    display(10.0);
}
