#include <iostream>

using namespace std;

/*
 1.
 抽象类中，在成员函数内可以调用纯虚函数，在构造函数/析构函数内部不能使用纯虚函数
 2.
 如果一个类从抽象类派生而来，它必须实现了基类中所有的纯虚函数，才能称为非抽象类
*/

class A {
   public:
    virtual void f() = 0;  // 纯虚函数
    void g() {             // 可以调用{
        this->f();
    }
    A() {}  // 不能调用
};

class B : public A {
   public:
    void f() {
        cout << "B:f()" << endl;
    }
};

int main() {
    B b;  // this 特指调用对象
    b.g();
    return 0;
}