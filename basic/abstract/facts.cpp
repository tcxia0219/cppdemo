#include <iostream>

using namespace std;

class Base {  // 抽象类，至少包含一个纯虚函数
   protected:
    int x;

   public:
    virtual void show() = 0;
    virtual void fun() = 0;
    int getX() {
        return x;
    }

    Base(int i) {
        x = i;
    }

    /*
    1. 构造函数不能是虚函数，而析构函数可以是虚析构函数
    2. 当基类指针指向派生类对象并删除对象时，可能希望调用适当的析构函数，如果析构函数不是虚的，则只能调用基类析构函数
    */
    virtual ~Base() {  // 虚析构函数
        cout << "Destructor: Base" << endl;
    }
};

class Derived : public Base {
    int y;

   public:
    void show() {
        cout << "In Derived \n";
    }

    void fun() {
        cout << "x = " << x << ", y = " << y << endl;
    }

    Derived(int i, int j) : Base(i) {
        y = j;
    }

    ~Derived() {
        cout << "Destructor : Derived" << endl;
    }
};

int main() {
    // Base b; // error 不能创建抽象类对象
    Base *base;  // 可以定义抽象类指针
    // Base *b = new Base(); // error
    // Base *b = new Derived(); // 抽象类的指针和引用 -> 由抽象类派生出来的类的对象
    Derived d(4, 5);
    d.show();
    d.fun();

    // Base *var = new Derived();
    // delete var;

    return 0;
}