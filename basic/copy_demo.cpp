#include <iostream>
#include <cstring>
#include <unistd.h>

using namespace std;

/**
 * @description: 拷贝构造函数
 */
class Student
{
private:
    int m_age;
    float m_score;
    string m_name;

public:
    Student()
    {
        cout << "Student()" << endl;
    }

    Student(int age, float score, string name) : m_age(age), m_score(score), m_name(name)
    {
        cout << "Student(int age, float score, string name)" << endl;
    }
    /**
     * @description: 
     * @param {const Student} &stu
     *  参数是当前类的引用: 如果不是当前类的引用，在调用拷贝构造函数时候，会将另外一个对象直接传递给形参，
     *                      本身就是一次拷贝，会再次调用拷贝构造函数，一直持续下去，陷入死循环
     *  const引用: 拷贝构造函数只是希望用其他对象来初始化当前对象，并不期望修改其他对象的数据；
     *              另外, 添加const限制后，可以将const对象和非const对象传递给形参
     *               如果没有const, 就不能将const对象传入形参，const类型不能转非const类型
     *              
     * @return {*}
     */
    Student(const Student &stu)
    {
        this->m_age = stu.m_age;
        this->m_score = stu.m_score;
        this->m_name = stu.m_name;
        cout << "Student(const Student &stu)" << endl;
    }

    ~Student()
    {
        cout << "~Student" << endl;
    }
};

void test1(Student stu)
{
    cout << "test(Student stu)" << endl;
}

void test2(const Student &stu)
{
    cout << "test(Student &stu)" << endl; //
}

Student test3(const Student &stu)
{
    cout << "test3()" << endl;
    Student stu5 = stu; // 拷贝构造函数
    return Student();
}

/**
 * @description: 深拷贝和浅拷贝
 * 深拷贝应用场景: 
 * 1. 如果一个类拥有指针类型的成员变量，绝大部分情况下就需要深拷贝
 *      只有这样，才能将指针指向的内容再复制出一份，让原有的对象和新对象相互独立.
 * 2. 在创建对象时进行一些预处理工作，比如统计创建过的对象数目、记录对象创建时间等，例如class Time;
 * 
 * 浅拷贝: 指针类型的变量只会拷贝地址值
 * 深拷贝: 将指针指向的内容拷贝到新的存储空间
 * 
 */
class Time
{
private:
    int m_a;
    int m_b;
    time_t m_time;      // 对象创建时间
    static int m_count; // 创建过的对象的数目

public:
    Time(int a = 0, int b = 0) : m_a(a), m_b(b)
    {
        m_count++;
        m_time = time((time_t *)NULL);
    }

    Time(const Time &t) // 拷贝构造函数
    {
        this->m_a = t.m_a;
        this->m_b = t.m_b;
        this->m_count++;
        this->m_time = time((time_t *)NULL);
    }

    int getCount() const
    {
        return m_count;
    }

    time_t getTime() const
    {
        return m_time;
    }
};

int Time::m_count = 0; // 静态成员变量必须在类外初始化

class Car
{
private:
    int m_price;
    char *m_name;
    void dcopy(const char *name = nullptr)
    {
        if (name == nullptr)
            return;
        m_name = new char[strlen(name) + 1]{}; // +1 字符串终止符'\0'
        strcpy(m_name, name);
    }

public:
    Car(int price = 0, const char *name = nullptr) : m_price(price)
    {
        dcopy(name);
    }

    Car(const Car &car)
    {
        dcopy(car.m_name);
    }

    ~Car()
    {
        if (m_name == nullptr)
            return;

        delete m_name;
        m_name = nullptr;
    }

    void display()
    {
        cout << "price: " << m_price << ", name: " << m_name << endl;
    }
};

/**
 * @description: 调用父类的构造函数
 * 子类定义的拷贝构造函数 不用父类构造函数初始化，将默认调用父类的默认构造函数
 * 1. Teacher(const Teacher &tea) : m_stu(tea.m_stu){} // 调用父类的默认构造函数
 *    Teacher(const Teacher &tea) : m_stu(tea.m_stu), Person(tea) {} // 子类的默认初始化并且进行拷贝
 */

class Person
{
public:
    int m_age;
    Person(int age = 20) : m_age(age) {}
    Person(const Person &person) : m_age(person.m_age) {}
};

class Teacher : public Person
{
public:
    int m_stu;
    Teacher(int age = 0, int stu = 0) : Person(age), m_stu(stu) {}
    Teacher(const Teacher &tea) : m_stu(tea.m_stu), Person(tea) {}
};

// 隐式构造
class Employee
{
private:
    int m_age;

public:
    Employee()
    {
        cout << "Employee() - " << this << endl;
    }
    Employee(int age) : m_age(age)
    {
        cout << "Employee(int) - " << this << endl;
    }
    Employee(const Employee &emp)
    {
        cout << "Employee(const Employee &emp) - " << this << endl;
    }
    ~Employee()
    {
        cout << "~Employee() - " << this << endl;
    }
};

void test4(Employee emp)
{
}

Employee test5(){
    return 60;
}

int main(int argc, char const *argv[])
{

    Employee e1(10);
    Employee e2 = 20;
    Employee e3 = e2;

    test4(50);

    test5();

    // const Student stu1(10, 110.1, "tcxia"); // 调用普通有参
    // Student stu2 = stu1;                    // 拷贝构造函数
    // Student stu3(stu1);                     // 拷贝构造函数
    // Student stu4;                           // 调用普通无参
    // stu4 = stu1;

    // test1(stu1); // 拷贝构造函数

    // test2(stu1); // 无构造函数调用

    // cout << endl;
    // test3(stu1); //  返回普通构造函数

    // Time t1(10, 20);
    // cout << "t1 count: " << t1.getCount() << " time: " << t1.getTime() << endl;

    // sleep(3);

    // Time t2 = t1;
    // cout << "t2 count: " << t2.getCount() << " time: " << t2.getTime() << endl;

    // Car car1(10, "bmw");
    // car1.display();

    // Car car2 = car1; // 拷贝构造函数 + 深拷贝
    // car2.display();

    // Car car3;
    // car3 = car1; // 浅拷贝
    // car3.display();

    // Teacher t1(10, 100);
    // Teacher t2(t1);
    // cout << t1.m_age << endl;

    // cout << t2.m_stu << endl;
    // cout << t2.m_age << endl;

    return 0;
}
