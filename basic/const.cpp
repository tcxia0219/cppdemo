#include <iostream>
using namespace std;


struct Date
{
	int year;
	int month;
	int day;
};

int main(int argc, char const *argv[])
{
	const int age = 20;
	// age = 30; // 不可被修改
	cout << age << endl;

	const Date d = {2021, 9, 22};
	// d.day = 25; // 不可修改
	Date dn = {2021, 8, 21};
	// d = dn;
	cout << "year: " << d.year << " month: " << d.month << " day:" << d.day << endl;

	const Date *pd = &dn;
	// pd->day = 26; // 不能修改
	cout << "year: " << pd->year << " month: " << pd->month << " day:" << pd->day << endl;


	int width = 10;
	int height = 20;

	// int const * const p5 = &width; // 不可修改*p4, 不可修改p4
	// cout << "p5: " <<  p5 << " *p5: " << *p5 << endl;

	const int *p1 = &width; // 可修改p1，不可修改*p1
	cout << "p1: " <<  p1 << " *p1: " << *p1 << endl;
	p1 = &height;
	// *p1 = height; // error
	cout << "p1: " <<  p1 << " *p1: " << *p1 << endl;
	cout << "width:" << width << endl;

	int const *p2 = &width; // 可修改p2，不可修改*p2
	cout << "p2: " <<  p2 << " *p2: " << *p2 << endl;
	p2 = &height; 
	// *p2 = height; // error
	cout << "p2: " <<  p2 << " *p2: " << *p2 << endl;
	cout << "width:" << width << endl;

	int * const p3 = &width; // 可修改*p3, 不可修改p3
	cout << "p3: " <<  p3 << " *p3: " << *p3 << endl;
	// p3 = &height; // error
	*p3 = height; 
	cout << "p3: " <<  p3 << " *p3: " << *p3 << endl;
	cout << "width:" << width << endl;

	int const * const p4 = &width; // 不可修改*p4, 不可修改p4
	cout << "p4: " <<  p4 << " *p4: " << *p4 << endl;
	// *p4 = height; // error
	// p4 = &height; // error
	// cout << "p4: " <<  p4 << " *p4: " << *p4 << endl;

	/*
	result:
	p1: 0x16f55b6f4 *p1: 10
	p1: 0x16f55b6f0 *p1: 20

	p2: 0x16f55b6f4 *p2: 10
	p2: 0x16f55b6f0 *p2: 20

	p3: 0x16f55b6f4 *p3: 10
	p3: 0x16f55b6f4 *p3: 20

	p4: 0x16f55b6f4 *p4: 20
	p4: 0x16f55b6f4 *p4: 20
	*/

	return 0;
}