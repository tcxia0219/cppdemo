#include <iostream>
#include <memory.h>
    
using namespace std;

struct Car{
    int m_price;
    Car(int price) : m_price{price + 1}{
        cout << "Car() - " << m_price << endl;
    }
    ~Car(){
        cout << "~Car()" << endl;
    }
};

class Person{
public:
    int m_age;
    Car *m_car;
    
    Person(){
        // memset(结构体/数组名, 用于替换的字符, 前n个字符)
        // memset(this, 0, sizeof(Person));

        m_age = 10;
        m_car = new Car(20);
        cout << "Person()" << endl;
        
    }
    ~Person(){
        delete m_car;
        cout << "~Person()" << endl;
    }
};

Person g_person; // 初始化为0， 全局区

void test(){
    Person person; // 成员变量未初始化, 栈空间

    Person *p1 = new Person; // 成员变量未初始化, 堆空间

    Person *p2 = new Person(); // 成员变量初始化为0, 堆空间
    
    cout << g_person.m_age << endl; // 0
    cout << p1->m_age << endl; // 0
    cout << p2->m_age << endl; // 0

    cout << p1 << endl; // 堆空间地址
    cout << &p1 << endl; // 栈空间地址

    delete p1;
    delete p2;
}



int main(int argc, char const *argv[])
{
    
    {
        Person person; // 内存泄露，该释放的资源没有得到释放
        // Car()
        // Person()
        // Car()
        // Person()
        // ~Person()
        // ~Person()

        Car car(10);
    }

    
    return 0;
}
