/*
 * @Author: tcxia
 * @Date: 2021-07-07 12:32:23
 * @FilePath: /cppwork/cppDemo/convert_demo.cpp
 */
#include <iostream>

using namespace std;

class Person
{
    virtual void run() {}
};

class Student : public Person
{
};

class Car
{
};

// 一般用于多态类型的转换，有运行时安全检测
void test_dynamic_cast()
{
    Person *p1 = new Person();    // 父类对象
    Person *p2 = new Student();   // 父类指针指向子类对象
    cout << "p1: " << p1 << endl; // p1: 0x745c20
    cout << "p2: " << p2 << endl; // p2: 0x745c40

    Car *c1 = (Car *)p1;
    Car *c2 = dynamic_cast<Car *>(p2);
    cout << "c1: " << c1 << endl; // c1: 0x745c20
    cout << "c2: " << c2 << endl; // c2: 0  安全检查

    Student *stu1d = dynamic_cast<Student *>(p1); // 不安全 父类对象赋值给子类指针， 会进行安全检查
    Student *stu2d = dynamic_cast<Student *>(p2); // 安全 指向子类指针赋值给子类对象
    cout << "stu1d: " << stu1d << endl;           // stu1d: 0
    cout << "stu2d: " << stu2d << endl;           // stu2d: 0x745c40

    Student *stu1 = (Student *)p1;    // 不安全
    Student *stu2 = (Student *)p2;    // 安全
    cout << "stu1: " << stu1 << endl; // stu1: 0x745c20
    cout << "stu2: " << stu2 << endl; // stu2: 0x745c40
}

void test_const_cast()
{
    const Person *p1 = new Person();
    Person *p2 = (Person *)p1;
    Person *p3 = const_cast<Person *>(p1); // 一般用于去除const属性，将const转换成非const
}

void test_static_cast()
{
    Person *p1 = new Person();
    const Person *p2 = p1; // 将非const转换const

    Person *p3 = new Person();
    Student *stu1 = static_cast<Student *>(p1);
    Student *stu2 = static_cast<Student *>(p3);
    // Car *car = static_cast<Car *>(p1); // 不能交叉转换（不是同一继承体系，无法转换）
    Car *car = dynamic_cast<Car *>(p1); // 可以交叉转换，但是进行安全检查
    cout << "p1: " << p1 << endl;       // p1: 0x1f01c20
    cout << "car: " << car << endl;     // 0
}

// 属于比较底层的强制转换，没有任何类型检查和格式转换，仅仅是简单的二进制数据拷贝
void test_reinterpret_cast()
{
    Person *p1 = new Person();
    Person *p2 = new Person();
    Student *stu1 = reinterpret_cast<Student *>(p1);
    Student *stu2 = reinterpret_cast<Student *>(p2);
    Car *car = reinterpret_cast<Car *>(p1); // 可以交叉转换
    cout << "p1: " << p1 << endl;           // p1: 0x1046c20
    cout << "p2: " << p2 << endl;           // p2: 0x1046c40
    cout << "stu1: " << stu1 << endl;       // stu1: 0x1046c20
    cout << "stu2: " << stu2 << endl;       // stu2: 0x1046c40
    cout << "car: " << car << endl;         // car: 0x1046c20

    int a = 10;                                //0A 00 00 00
    double d1 = a;                             // int ---> double: 0A 00 00 00 ---> 00 00 00 00 00 00 24 40
    double d2 = reinterpret_cast<double &>(a); // int ---> double: 0A 00 00 00 ---> 0A 00 00 00 cc cc cc cc

    // 将指针和整数进行互换
    // int类型报错，主要是因为x64系统指针类型占用8个字节
    long *p = reinterpret_cast<long *>(100);
    cout << sizeof(p) << endl; // 占用8个字节
    long num = reinterpret_cast<long>(p);
    cout << "p: " << p << endl;     // p: 0x64
    cout << "num: " << num << endl; // num: 100
}

int main(int argc, char const *argv[])
{

    test_const_cast();
    test_static_cast();
    test_dynamic_cast();
    test_reinterpret_cast();
    return 0;
}
