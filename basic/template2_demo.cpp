#include <iostream>
    
using namespace std;

template <typename T>
class HasFriend{
    template <typename U>
    friend void call(const U&);
private:
    void show(){
        cout << "Call private method" << endl;
    }
};

int main(int argc, char const *argv[])
{
    
    return 0;
}
