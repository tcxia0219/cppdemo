#include<iostream>
#include<vector>
#include<list>
#include<array>
#include<algorithm>

using namespace std;

class testDemo
{
private:
    int m_num;

public:
    // 普通构造函数
    testDemo(int num) : m_num(num)
    {
        cout << "testDemo(int num)" << endl;
    }

    // 拷贝构造函数
    testDemo(const testDemo &other) : m_num(other.m_num)
    {
        cout << "testDemo(const testDemo &other)" << endl;
    }

    // 移动构造函数
    testDemo(testDemo &&other) : m_num(other.m_num)
    {
        cout << "testDemo(testDemo&& other)" << endl;
    }

    testDemo &operator=(const testDemo &other);
};

testDemo &testDemo::operator=(const testDemo &other)
{
    this->m_num = other.m_num;
    return *this;
}

int main(int argc, const char** argv) {

    vector<int> v{1, 2, 3, 4, 5};
    // 添加数据
    for (int i = 0; i < 10; i++)
    {
        v.push_back(i);
    }

    // 遍历数据1 正向迭代器
    vector<int>::iterator it;
    for ( it = v.begin(); it != v.end(); it++)
    {
        cout << *it << endl;
    }

    // 遍历数据3 
    for (it = v.begin(); it < v.end(); it++){
        cout << *it << endl;
    }

    // 遍历数据2
    for (int i = 0; i < v.size(); i++)
    {
        cout << v[i] << endl;
    }

    list<int> l;
    list<int>::const_iterator li; // 常量正向迭代器
    for (li = l.begin(); li != l.end(); ++li){
        cout << *li << endl;
    }

    // for (li = l.begin(); li < l.end(); ++li) // 不合法 双向迭代器不支持'<'
    // {
    //     cout << *li << endl;
    // }

    // for (int i = 0; i < l.size(); i++)
    // {
    //     cout << *i << endl; // 不合法 双向迭代器不支持随机下标访问
    // }

    cout << "===== array demo =====" << endl;
    array<double, 10> values1;   // 不会默认初始化操作
    array<double, 10> values2{}; // 初始化为0.或默认元素等效的值
    array<double, 10> values3{0.5, 0.3, 2.1}; // 初始化前三个值，后面默认都是0.

    array<int, 10> values4{};
    for (int i = 0; i < 5; i++){
        values4.at(i) = i; // 使用 at 关键字 访问元素
    }

    cout << get<3>(values4) << endl; // get重载获取指定位置元素，实参必须是一个确定的常量表达式
    cout << "data() ---> " << *(values4.data()) + 4<<endl; // data() 得到容器首个元素的地址

    // 容器不为空，则输出容器中所有的元素
    if(!values4.empty()){
        // begin 返回第一个元素的正向迭代器
        // end 返回最后一个元素之后一个位置的正向迭代器
        for (auto val = values4.begin(); val < values4.end(); val++){
            cout << *val << " ";
        }
        cout << endl;
    }

    // 显示使用迭代器初始化values的值
    array<int, 5> values5;
    int h5 = 1;
    auto first5 = values5.begin();
    auto end5 = values5.end();

    // 初始化 values 容器为{1, 2, 3, 4, 5}
    while (first5 != end5)
    {
        *first5 = h5;
        ++first5;
        h5++;
    }
    first5 = values5.begin();
    while (first5 != end5)
    {
        cout << *first5 << " ";
        ++first5;
    }
    cout << endl;

    array<int, 5> values6{1, 2, 3, 4, 5};
    int h6 = 1;
    auto first6 = values6.cbegin(); // 返回const类型的正向迭代器，不能对存储的元素进行修改
    auto last6 = values6.cend();

    // *first = 10;  // const类型，无法修改

    while (first6 != last6)
    {
        cout << *first6 << " ";
        ++first6;
    }
    cout << endl;

    array<int, 5> values7;
    int h7 = 1;
    auto first7 = values7.rbegin(); // 指向最后一个元素的方向迭代器
    auto last7 = values7.rend(); // 指向第一个元素之前一个位置的迭代器

    // 初始化容器为{1, 2, 3, 4, 5}
    while (first7 != last7)
    {
        *first7 = h7;
        ++first7; // 向左移动一位 
        h7++;
    }
    // 遍历容器
    first7 = values7.rbegin();
    while (first7 != last7)       
    {
        cout << *first7 << " ";
        ++first7; // 向左移动一位
    }
    cout << endl;

    // values7[3] = values7[2] + values7[10]; // !这种访问越界，编译器不会检测到
    // values7.at(7) = values7.at(2) + values7.at(10); // 和上面一致，但是会做边界检测，抛出异常

    array<int, 5> value11;
    array<int, 5> value12;
    
    // 初始化value11
    for (size_t i = 0; i < value11.size(); i++){
        value11.at(i) = i;
    }

    // 遍历形式
    int initvalue = 10;
    for (auto &value : value12) // 引用遍历
    {
        value = initvalue;
        initvalue++;
    }

    cout << "===== vector demo =====" << endl;
    vector<double> v_values1;
    /*
    增加容器的容量 
    1. 如果在此语句之前已经大于或等于20个元素，这条语句什么也不做；
    2. 不会影响已经存储的元素, 也不会生成新的元素
    3. 之前创建好的迭代器可能会失效，可能会被复制或移动到新的内存地址
    */
    v_values1.reserve(20); // 未初始化
    for (auto &v : v_values1){
        cout << v << " ";
    }
    cout << endl; //

    vector<int> v_values2{1, 2, 3, 5, 6}; // 创建并且初始化

    vector<int> v_values3(20); // 创建并且初始化为0

    vector<int> v_values4(20, 5); // 创建并且初始化为0.1

    int num = 10;
    int v_num = 5;
    vector<int> v_values5(num, v_num); // 创建并且初始化(使用变量)

    vector<char> v_values6(5, 'c');
    vector<char> v_values7(v_values6); // 使用已存在的vector创建新的vector

    int array[] = {1, 2, 3};
    vector<int> v_values8(array, array + 2); // 保存了{1, 2}, array是数组的首地址

    vector<int> v_values9{1, 2, 3, 4, 5};
    vector<int> v_values10(begin(v_values9), begin(v_values9) + 3); // 保存{1, 2, 3}

    for (auto &v : v_values10) // 引用遍历
    {
        cout << v << " ";
    }
    cout << endl;

    vector<char> v_value11;
    v_value11.push_back('S');
    v_value11.push_back('T');
    v_value11.push_back('L');

    cout << "size of vector: " << v_value11.size() << endl;

    // 迭代器遍历
    for (auto i = v_value11.begin(); i != v_value11.end(); i++){
        cout << *i << " ";
    }
    cout << endl;

    v_value11.insert(v_value11.begin(), 'C');
    cout << v_value11.at(1) << endl;

    vector<int> v_value12{1, 2, 3, 4, 5};
    auto v_first12 = v_value12.begin(); // auto v_first12 = begin(v_value12)
    auto v_last12 = v_value12.end(); // auto v_last12 = end(v_last12)
    while (v_first12 != v_last12){
        cout << *v_first12 << " ";
        ++v_first12; // 先加1，在赋值
    }

    vector<int> v_value13{1, 2, 3, 4, 5};
    auto v_first13 = v_value13.cbegin();
    auto v_last13 = v_value13.cend();

    // *v_first13 = 15; // 表达式必须是可修改的左值

    vector<int> v_value14{ 1, 2, 3, 4, 5};
    auto v_first14 = v_value14.rbegin(); // 指向最后一个元素的迭代器（指针）
    auto v_last14 = v_value14.rend(); // 指向第一个元素的下一个位置的迭代器（指针）

    vector<int> v_value15;
    int val = 1;
    // 什么都没输出，对于空的vector，begin和end成员返回的迭代器是等价的，即指向同一个位置
    // 一般对于空的vector, 通过push_back或者 resize来初始化
    for (auto first = v_value15.begin(); first < v_value15.end(); ++first, ++val){
        *first = val;
        cout << *first << "end";
    }
    cout << endl;

    /*
        重新申请内存，首地址可能会发生变化，因此依然使用之前的迭代器是错误的，需要重新创建新的迭代器
    */
    vector<int> v_value16{1, 2, 3};
    cout << "首地址: " << v_value16.data() << endl; // 0x175e370

    auto v_first16 = v_value16.begin();
    auto v_last16 = v_value16.end();

    v_value16.reserve(20); // 增加vector的容量
    cout << "首地址: " << v_value16.data() << endl; // 0x175e390 data(): 返回vector的首元素地址

    // 创建新的迭代器
    v_first16 = v_value16.begin();
    v_last16 = v_value16.end();

    vector<int> v_value17{1, 2, 3};
    cout << v_value17.at(2) << endl; // 获取下标2的元素

    cout << v_value17.front() << endl; // 首元素
    cout << v_value17.back() << endl; // 尾元素

    v_value17.front() = 10; // 可修改
    v_value17.back() = 20;

    for(auto &v : v_value17){
        cout << v << " ";
    }
    cout << endl;

    vector<int> v_value18{};

    /*
    底层实现不一致
    push_back 会先创建这个元素，然后再将这个元素拷贝或移动到容器中，如果是拷贝，事后会自行销毁先前创建的这个元素
    emplace_back 直接在容器尾部创建这个元素，省去了拷贝或移动元素的过程
    */
    v_value18.push_back(1); //
    v_value18.push_back(2);

    v_value18.emplace_back(3);
    v_value18.emplace_back(4);



    vector<testDemo> demo1;
    demo1.emplace_back(2); // testDemo(int num) 只调用了普通构造函数, 建议使用

    cout << "====" << endl;

    vector<testDemo> demo2;
    // testDemo(int num)  先调用普通构造函数
    // testDemo(testDemo&& other) 再调用移动构造函数，如果没有移动构造函数, 调用拷贝构造函数 testDemo(const testDemo &other)
    demo2.push_back(3);
    cout << "======" << endl;

    vector<int> v_value19{1, 2};
    v_value19.insert(v_value19.begin() + 1, 3); // {1, 3, 2}
    v_value19.insert(v_value19.end(), 2, 5); // {1, 3, 2, 5, 5} // 插入n个元素值为5

    std::array<int, 3> test_v{7, 8, 9};
    v_value19.insert(v_value19.begin(), test_v.begin(), test_v.end()); // {1, 3, 2, 5, 5, 7, 8, 9}

    v_value19.insert(v_value19.end(), {10, 11}); // {1, 3, 2, 5, 5, 7, 8, 9, 10, 11}

    // 每次只能插入一个元素，而不是多个
    v_value19.emplace(v_value19.begin(), 12); // {12, 1, 3, 2, 5, 5, 7, 8, 9, 10, 11}

    // 参考以上 push_back 和 emplace_back
    /*
        testDemo(int num)
        testDemo(testDemo&& other)
    */
    vector<testDemo> demo3{};
    demo3.insert(demo3.begin(), testDemo(1));

    cout << "======" << endl;
    // testDemo(int num)
    vector<testDemo> demo4{};
    demo4.emplace(demo4.begin(), 2);  // 推荐使用

    cout << "======" << endl;

    vector<int> v_value20{1, 2, 3, 4, 5};
    cout << "before size: " << v_value20.size() << endl; // 5
    cout << "before capacity: " << v_value20.capacity() << endl; // 5

    v_value20.pop_back();
    cout << "after size: " << v_value20.size() << endl; // 4
    cout << "after capacity: " << v_value20.capacity() << endl; // 5

    auto iter = v_value20.erase(v_value20.begin() + 2); // 删除 元素3
    cout << "after size: " << v_value20.size() << endl;         // 3
    cout << "after capacity: " << v_value20.capacity() << endl; // 5

    cout << *iter << endl; // 迭代器指向元素 4


    return 0;
}

/*
    申请新的堆空间(当原来申请的堆空间不够使用时)
    int *p = new int[n]; // 原来申请的堆空间

    int *temp = new int[m]; // 新申请的堆空间
    memecpy(temp, p, sizeof(int) * n); // 拷贝原来的数据到新的堆空间
    delete[] p; // 释放原来的堆空间
    p = temp;
*/