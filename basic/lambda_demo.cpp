#include <iostream>

using namespace std;

/**
 * @description: Lambda表达式
 * 完整结构: [capture list](params list)mutable exception -> return type {function body}
 * capture list: 捕获外部变量列表
 * params list: 形参列表，不能使用默认参数，不能省略参数名
 * mutable: 用来表示是否可以更改捕获的变量
 * exception: 异常设定
 * return type: 返回值类型
 * function body: 函数体
 * 
 * 可以省略的部分
 *  1. [capture list](params list) -> return type {function body}
 *  2. [capture list](params list){function body}
 *  3. [capture list]{function body}
 */

// 函数作为函数的参数传入
int exec(int a, int b, int(*func)(int, int)){
    if (func == nullptr) return 0;
    return func(a, b);
}

void test_lambda(){
    // lambda表达式形式
    // 1
    auto func1 = []()
    {
        cout << "lambda - " << endl;
    };
    func1();

    // 2
    int (*p1)(int, int) = [](int v1, int v2) -> int
    {
        return v1 + v2;
    };

    cout << p1(10, 20) << endl;

    // 3
    auto p2 = [](int v1, int v2)
    {
        return v1 + v2;
    };

    cout << p2(11, 21) << endl;

    // 4
    auto p3 = [](int v1, int v2)
    {
        return v1 + v2;
    }(12, 22);
    cout << p3 << endl;


    cout << exec(20, 10, [](int v1, int v2) {return v1 + v2;}) << endl;
    cout << exec(20, 10, [](int v1, int v2) {return v1 - v2;}) << endl;
    cout << exec(20, 10, [](int v1, int v2) {return v1 * v2;}) << endl;
    cout << exec(20, 10, [](int v1, int v2) {return v1 / v2;}) << endl;
}

void test_catch(){
    // 捕获
    /**
     * 值捕获: 外部修改变量，内部变量值不发生改变
     * 地址捕获: 外部修改变量，内部变量值发生改变 
     */
    int a = 10;
    int b = 20;
    // a是地址捕获，b是值捕获
    auto func2 = [&a, b]{
        cout << a << endl; // 
        cout << b << endl; // 
    };
    a = 11;
    b = 21;
    func2(); // 10 20 只会输出原有的变量，修改变量并不管用

    // 隐式捕获
    // "=" 代表值捕获  "&"代表地址捕获
    auto func3 = [=]{
        cout << a << endl; // 
        cout << b << endl; // 
    };
    a = 12;
    b = 22;
    func3();

    auto func4 = [=, &a]{
        cout << a << endl; // 
        cout << b << endl; // 
    };
    a = 13;
    b = 23;
    func4();

}

int main(int argc, char const *argv[])
{
    // mutable 可以直接修改捕获变量的值, 但是仅限lambda表达式内部使用，外部变量并没有改变
    int a = 10;
    auto func = [a]() mutable{
        // int b = a;
        // cout << ++b << endl; // 无法直接修改a的值

        cout << ++a << endl;
    };
    func(); // 11
    cout << a << endl; // 10

    return 0;
}
