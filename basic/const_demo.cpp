#include <iostream>
using namespace std;

/*
    const:
        const 成员变量: 初始化只能通过构造函数的初始化列表; 非static的const成员还可以在初始化列表中初始化
        const 成员函数: 可以使用类中的所有成员变量，但是不能修改它们的值，主要为了保护数据
            1. 内部不能修改非static成员变量
            2. 内部只能调用const成员函数、static成员函数
            3. 非const成员函数可以调用const成员函数
        const 对象: 只能调用const成员（包括const成员变量和成员函数）和 static成员函数
*/

/*
    引用:
        1. 作为函数参数: 在定义或声明指定参数为引用的形式，这样修改了形参的数据，实参也会被修改
        2. 作为函数返回值: 
*/

int &plus10(int &r)
{
    // int m = r + 10; // 不能返回局部变量, 函数在栈内，自动分配和释放
    r += 10;
    return r;
}

void test_plus(){
    int n1 = 10;
    int n2 = plus10(n1);
    cout << n2 << endl;

    // int &n3 = plus10(n1);
    // int &n4 = plus10(n3);
    // cout << n3 << endl;
    // cout << n4 << endl;
}


void test(){
    int age = 10;
    int height = 20;

    // p0不是常量， *p0是常量
    const int *p0 = &age;
    p0 = &height;

    // p1不是常量， *p1是常量
    int const *p1 = &age;
    p1 = &height;

    // p2是常量， *p2不是常量
    int *const p2 = &age;
    // p2 = &height; // 报错， 不能赋值

    // p3是常量， *p3也是常量
    const int *const p3 = &age;

    // p4是常量， *p4也是常量
    int const *const p4 = &age;
}

struct Student
{
    int age;
};

void test_class(){
    Student stu1 = {10};
    Student stu2 = {20};

    const Student *pStu1 = &stu1;
    // *pStu1 = stu2; // 报错， *pStu1是const，不能被修改
    // (*pStu1).age = 30; // 报错
    // pStu1->age = 40; // 报错
    // pStu1 = &stu2; // 正确

    Student *const pStu2 = &stu2;
    // pStu2->age = 30; // 正确, pStu2是const, 不可修改
    // (*pStu2).age = 30; // 正确
    // *pStu2 = stu1; // 正确
    // pStu2 = &stu1; // 报错
}

void test1(){
    int age = 10;
    int height = 20;

    // 定义了一个age的引用，ref相当于是age的别名
    int &ref = age;
    int &ref1 = ref; // 正确
    ref = height; // 对引用做计算，就是对引用的对象进行计算

    // &ref = height; // 报错，不能修改

    cout << age << endl; // 20
    cout << ref << endl; // 20
}


// 并未真正交换两个值， 添加引用并未构成重载
// void swap(int v1, int v2){
//     int temp = v1;
//     v1 = v2;
//     v2 = temp;
// }

// 交换两个值
// void swap(int &v1, int &v2){
//     int temp = v1;
//     v1 = v2;
//     v2 = temp;
// }

// 交换两个值
void swap(int *v1, int *v2){
    int *temp = v1;
    v1 = v2;
    v2 = temp;
}


struct Date
{
    int year;
    int month;
    int day;
};

void test2(){
    Date d = {2021, 7, 12};
    
    Date &ref = d;
    ref.month = 10; // 直接访问

    int age = 10;
    int height = 30;

    int *p = &age;
    int *&ref1 = p; // ref1相当于*p的别名
    *ref1 = 20;
    ref1 = &height; // 修改指针

    int array[] = {1, 2, 3};
    int (&ref2)[3] = array; // 数组的引用
    int * const &ref3 = array; // 数组的引用
    
    cout << ref2[0] << endl; // 1
    cout << ref3[1] << endl; // 2

    int *prev;
    // 指针数组，数组里面可以存在3个int *
    int *array1[3] = {prev, prev, prev};
    // 用于指向数组的指针
    int (*array2)[3];
}

void test3(){
    int height = 20;
    int age = 30;

    // ref1不能修改指向，但是可以通过ref1间接修改指向的变量
    int & const ref1 = age; // ‘const’ qualifiers cannot be applied to ‘int&’
    ref1 = 30;
    // &ref1 = height;
    // cout << age << endl;

    int const &ref2 = age;
    // ref2 = 40; // 报错，不能修改ref2指向的变量
    // &ref2 = height;
    int &ref3 = age;
    
    ref3 = 50;
    cout << age << endl; // 50
}


// 1
int sum(int &v1, int &v2){
    cout << "int sum(int &v1, int &v2)" << endl;
    return v1 + v2;
}

// 2 与1构成重载
int sum(const int &v1, const int &v2){
    cout << "int sum(const int &v1, const int &v2)" << endl;
    return v1 + v2;
}

// 3 与其他两个不构成重载
// int sum(const int v1, const int v2){
//     return v1 + v2;
// }



int main()
{
    int a = 10;
    int b = 20;
    int c = sum(a, b); // 1存在，调用1， 不存在，调用2
    cout << c << endl;

    const int a1 = 20;
    const int b1 = 30;
    int c1 = sum(a1, b1); // 只能调用2
    cout << c1 << endl;

    
    return 0;
}