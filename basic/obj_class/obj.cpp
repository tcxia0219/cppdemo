#include <iostream>
using namespace std;

struct Person{
	int m_id;
	int m_age;
	int m_height;

	void display(){
		cout << "id = " << m_id
			<< ", age = " << m_age
			<< ", height = " << m_height << endl;
	}
};


int main(int argc, char const *argv[])
{
	Person p; // 对象内存在栈空间
	p.m_id = 1;
	p.m_age = 2;
	p.m_height = 3;
	p.display();

	cout << "&p == " << &p << endl;
	cout << "&p.m_id == " << &p.m_id << endl;
	cout << "&p.m_age == " << &p.m_age << endl;
	cout << "&p.m_height == " << &p.m_height << endl;
	/*
	&p == 0x16dacf654
	&p.m_id == 0x16dacf654
	&p.m_age == 0x16dacf658
	&p.m_height == 0x16dacf65c
	*/
	return 0;
}