#include <iostream>

using namespace std;


struct Person{
	int m_age;
	void run(){
		cout << "run() - age: " << m_age << endl;
	}
};

class Student{
public:
	int m_score;
	void run(){
		cout << "run() - score: " << this->m_score << endl;
	}
};


class Teacher{
public:
	int m_id;
	int m_age;
	int m_score;
	void SetAge(int age){
		this->m_age = age;
	}

	int GetAge(){
		return this->m_age;
	}


	void display(){
		cout << "m_id:" << this->m_id << endl;
		cout << "m_age:" << this->m_age << endl;
		cout << "m_score:" << this->m_score << endl;
	}
};

int main(int argc, char const *argv[])
{
	Person p;
	p.m_age = 10;
	p.run();

	Student s;
	s.m_score = 100;
	s.run();

	Person *pn = &p; //通过指针访问
	pn->m_age = 50;
	pn->run();


	Teacher t;
	Teacher *pt = &t;
	pt->m_id = 1;
	pt->m_age = 10;
	pt->m_score = 100;
	pt->display();

	Teacher *ptn = (Teacher *)&t.m_age;
	ptn->m_id = 2;
	// ptn->m_age = 11;
	ptn->m_score = 101;
	ptn->display(); 
	/*
	m_id:2
	m_age:100
	m_score:101
	*/



	
	return 0;
}