#include <iostream>

using namespace std;


int sum(int v1 = 3, int v2 = 4){
	return v1 + v2;
}


void test(int a){
	cout << "test(int a) - " << a << endl;
}

void func(int v1, void(*p)(int) = test){
	p(v1);
}

int main(int argc, char const *argv[])
{
	cout << sum(1, 4) << endl;
	cout << sum(2, 4) << endl;
	cout << sum(3, 4) << endl;


	cout << sum(1) << endl;
	cout << sum(2) << endl;
	cout << sum(3) << endl;

	cout << sum(2, 3) << endl;

	func(30);
	func(20, test);

	return 0;
}


