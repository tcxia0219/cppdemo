#include <iostream>
    
using namespace std;

// 第三方框架入库可能使用C语言写的

/**
 * 如果函数同时有声明和实现，要让函数声明被extern "C"修饰，实现可以不修饰
 * 
 * 头文件
 * 1. 经常使用#ifndef, #define, #endif来防止头文件被重复包含
 * 2. #pragma once可以防止整个文件的内容被重复包含
 * 
 * 区别:
 *  1. #idndef, #define, #endif受C/C++标准的支持，不受编译器的任何限制
 *  2. 有些编译器不支持#pragma once，兼容性不够好
 *  3. #ifndef, #define, #endif可以针对一个文件中的部分代码，而#pragma once只能针对整个文件
 */

// 第一种写法
extern "C" void func(){}

extern "C" void func(int v){}

// 第二种写法
extern "C" {
    void func(){}
    void func(int v){}
}


int main(int argc, char const *argv[])
{
    int a = 10;
    func(a); // 报错，C语言没有重载
    return 0;
}
